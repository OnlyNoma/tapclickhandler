<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MainControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertEquals('AppBundle\Controller\MainController::indexAction',
            $client->getRequest()->attributes->get('_controller'));

        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
    }
}