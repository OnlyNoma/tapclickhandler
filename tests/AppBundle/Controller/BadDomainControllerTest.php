<?php

namespace AppBundle\Tests\Controller;


use Liip\FunctionalTestBundle\Test\WebTestCase;

class BadDomainControllerTest extends WebTestCase
{
    public function testShowBadDomains()
    {
        $client = static::createClient();
        $client->request('GET', '/bad-domains/show');
        $this->assertEquals('AppBundle\Controller\BadDomainController::showBadDomainsAction',
            $client->getRequest()->attributes->get('_controller'));

        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
    }

    public function testAddBadDomainsRedirectToForm()
    {
        $client = static::createClient();
        $client->request('GET', '/bad-domains/add/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testAddBadAjaxQuery()
    {
        $client = $this->makeClient();
        $client->request(
            'POST',
            '/bad-domains/add/new',
            ['domain-name' => 'http://localhost:8000'],
            [],
            ['HTTP_X-Requested-With' => 'XMLHttpRequest']
        );

        $jsonResponse = json_decode($client->getResponse()->getContent(), true);

        $this->assertStatusCode(200, $client);
        $this->assertNotEmpty($jsonResponse);

        $this->assertArrayHasKey('success', $jsonResponse);
        $this->assertArrayHasKey('message', $jsonResponse);
    }
}