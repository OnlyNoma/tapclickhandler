<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\Click;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class ClickControllerTest extends WebTestCase
{
    public function testClickSuccess()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        $container = static::$kernel->getContainer();

        $clickUniqueId = '01329dcd-2c21-11e8-83df-0242ac170002';

        $click = $container->get('app.click_service')->getById($clickUniqueId);

        $this->assertInstanceOf(Click::class, $click);
    }
}