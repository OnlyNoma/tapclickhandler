<?php

namespace AppBundle\Tests\Services;


use AppBundle\Entity\Click;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ClickServiceTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        $this->container = static::$kernel->getContainer();
    }

    public function testCreateClickSuccess()
    {
        $clickInfo = [
            'ip' => '127.0.0.1',
            'param1' => 'SomeParam1Value',
            'param2' => 'SomeParam2Value',
            'userAgent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36',
            'ref' => 'http://localhost:8000/links/list',
        ];

        $testClickService = $this->container->get('app.click_service');

        $this->assertInstanceOf(Click::class, $testClickService->createClick($clickInfo));
    }

    public function testCreateClickFail()
    {
        $clickInfo = [
            'ip' => '127.0.0.1',
            'param1' => 'SomeParam1Value',
            'param2' => 'SomeParam2Value',
            'userAgent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36',
            'ref' => 'http://localhost:8000/links/list',
        ];


        $testClickService = $this->container->get('app.click_service');

        $this->expectException(\Exception::class);

        $testClickService->createClick($clickInfo);
    }

    public function testIsClickAlreadyExistSuccess()
    {
        $testClickService = $this->container->get('app.click_service');

        $clickInfo = [
            'ip' => '172.23.0.1',
            'param1' => 'param3_1',
            'param2' => 'param3_1',
            'userAgent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36',
            'ref' => 'http://localhost:8000/links/list',
        ];

        $isAlreadyExist = $testClickService->isClickAlreadyExist($clickInfo);

        $this->assertEquals(true, $isAlreadyExist);
    }

    public function testGetByIdSuccess()
    {
        $testClickService = $this->container->get('app.click_service');

        $customUniqueId = '01329dcd-2c21-11e8-83df-0242ac170001';

        $click = $testClickService->getById($customUniqueId);

        $this->assertInstanceOf(Click::class, $click);
    }

    public function testGetByIdFail()
    {
        $testClickService = $this->container->get('app.click_service');

        $customUniqueId = 'SomeFailUniqueId';

        $this->expectException(NotFoundHttpException::class);

        $testClickService->getById($customUniqueId);
    }

    public function testGetIdByInfoSuccess()
    {
        $testClickService = $this->container->get('app.click_service');

        $clickInfo = [
            'ip' => '172.23.0.1',
            'param1' => 'param3_1',
            'param2' => 'param3_1',
            'userAgent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36',
            'ref' => 'http://localhost:8000/links/list',
        ];

        $clickId = $testClickService->getIdByInfo($clickInfo);

        $this->assertEquals('01329dcd-2c21-11e8-83df-0242ac170001', $clickId);
    }

    public function testGetIdByInfoFail()
    {
        $testClickService = $this->container->get('app.click_service');

        $clickInfo = [
            'ip' => '172.0.0.1',
            'param1' => 'custom',
            'param2' => 'custom2',
            'userAgent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36',
            'ref' => 'what is ref?',
        ];

        $this->expectException(NotFoundHttpException::class);

        $testClickService->getIdByInfo($clickInfo);
    }

    public function testIncreaseErrorCountFail()
    {
        $testClickService = $this->container->get('app.click_service');

        $customUniqueId = 'SomeFailUniqueId';

        $this->expectException(NotFoundHttpException::class);

        $testClickService->increaseErrorCount($customUniqueId);
    }

    public function testIncreaseErrorCountSuccess()
    {
        $testClickService = $this->container->get('app.click_service');

        $customUniqueId = '01329dcd-2c21-11e8-83df-0242ac170001';

        $testClickService->increaseErrorCount($customUniqueId);

        $this->assertTrue(true);
    }

    public function isBadDomainFail()
    {
        $testClickService = $this->container->get('app.click_service');

        $clickInfo = [
            'ip' => '172.0.0.1',
            'param1' => 'custom',
            'param2' => 'custom2',
            'userAgent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36',
            'ref' => 'what is ref?',
        ];

        $isClickHasBadDomain = $testClickService->isBadDomain($clickInfo['ref']);

        $this->assertEquals(true, $isClickHasBadDomain);
    }

    public function isBadDomainFailWithNull()
    {
        $testClickService = $this->container->get('app.click_service');

        $isClickHasBadDomain = $testClickService->isBadDomain(null);

        $this->assertEquals(true, $isClickHasBadDomain);
    }

    public function isBadDomainSuccess()
    {
        $testClickService = $this->container->get('app.click_service');

        $testCorrectRef = 'http://localhost:8000';

        $isClickHasBadDomain = $testClickService->isBadDomain($testCorrectRef);

        $this->assertEquals(false, $isClickHasBadDomain);
    }
}