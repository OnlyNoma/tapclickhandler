<?php

namespace AppBundle\Tests\Services;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class BadDomainServiceTest extends WebTestCase
{
    public function testIsDomainExistFail()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        $badDomainService = static::$kernel->getContainer()->get('app.bad_domain_service');

        $testDomainName = 'SomeFailDomainName';

        $domain = $badDomainService->isDomainExist($testDomainName);

        $this->assertEquals(false, $domain);
    }
}