<?php

namespace AppBundle\Services;


use AppBundle\Entity\BadDomains;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BadDomainService
{

    private $entityManager;
    private $container;

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    public function isDomainExist($domainName)
    {
        if (empty($domainName)) {
            throw new \Exception('Domain Name is empty');
        }

        $domain = $this->entityManager->getRepository('AppBundle:BadDomains')
            ->findOneBy(['name' => $domainName]);

        if (empty($domain)) {
            return false;
        }

        return true;
    }

    public function createNewBadDomain($domainName)
    {
        if (empty($domainName)) {
            throw new \Exception('Domain Name is empty');
        }

        $newBadDomain = new BadDomains();
        $newBadDomain->setName($domainName);

        $this->entityManager->persist($newBadDomain);
        $this->entityManager->flush();
    }
}