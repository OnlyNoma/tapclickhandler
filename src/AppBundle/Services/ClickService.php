<?php

namespace AppBundle\Services;

use AppBundle\Entity\Click;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ClickService
{
    private $container;
    private $entityManager;

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    public function isClickAlreadyExist(array $clickParams)
    {
        $clicks = $this->entityManager->getRepository('AppBundle:Click')
            ->findOneBy($clickParams);
        
        if (empty($clicks)) {
            return false;
        }
        
        return true;
    }

    public function getById($clickUniqueId)
    {
        $clickObject =  $this->entityManager->getRepository('AppBundle:Click')
            ->findOneBy(['id' => $clickUniqueId]);

        if (empty($clickObject)) {
            throw new NotFoundHttpException('Click object not found');
        }

        return $clickObject;
    }

    public function getIdByInfo($clickInfo)
    {
        $clickObject =  $this->entityManager->getRepository('AppBundle:Click')
            ->findOneBy($clickInfo);

        if (empty($clickObject)) {
            throw new NotFoundHttpException('Click object not found');
        }

        return $clickObject->getId();
    }

    public function createClick($clickParams)
    {
        $click = $this->entityManager->getRepository('AppBundle:Click')
            ->findBy($clickParams);

        if (!empty($click)) {
            throw new \Exception('Click already exist');
        }

        $click = new Click();
        $click->setIp($clickParams['ip']);
        $click->setParam1($clickParams['param1']);
        $click->setParam2($clickParams['param2']);
        $click->setUserAgent($clickParams['userAgent']);
        $click->setRef($clickParams['ref']);

        $this->entityManager->persist($click);
        $this->entityManager->flush();

        return $click;
    }

    public function increaseErrorCount($clickUniqueId)
    {
        $click = $this->entityManager->getRepository('AppBundle:Click')
            ->findOneBy(['id' => $clickUniqueId]);

        if (empty($click)) {
            throw new NotFoundHttpException('Click object not fount');
        }

        $click->setError($click->getError() + 1);

        $this->entityManager->persist($click);
        $this->entityManager->flush();
    }

    public function isBadDomain($ref)
    {
        $badDomain = $this->entityManager->getRepository('AppBundle:BadDomains')
            ->findOneBy(['name' => $ref]);

        if (empty($badDomain)) {
            return false;
        }

        return true;
    }

    public function setBadDomain($clickUniqueId)
    {
        $click = $this->entityManager->getRepository('AppBundle:Click')
            ->findOneBy(['id' => $clickUniqueId]);

        if (empty($click)) {
            throw new NotFoundHttpException('Click object not fount');
        }

        $click->setBadDomain(1);

        $this->entityManager->persist($click);
        $this->entityManager->flush();
    }
}