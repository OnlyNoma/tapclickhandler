<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Click;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ClickController extends Controller
{
    /**
     * @Route("/click", name="click_request")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function handleAction(Request $request)
    {
        $clickParams = [
            'userAgent' => $request->headers->get('user-agent'),
            'ip' => $request->getClientIp(),
            'ref' => $request->headers->get('referer'),
            'param1' => $request->query->get('param1'),
        ];

        $clickService = $this->get('app.click_service');

        if ($clickService->isClickAlreadyExist($clickParams)) {

            /** @var Click $clickObject */
            $clickId = $clickService->getIdByInfo($clickParams);

            if ($clickService->isBadDomain($clickParams['ref'])) {
                $clickService->setBadDomain($clickId);
            }

            return $this->redirectToRoute('click_error', [
                'clickUniqueId' => $clickId,
            ]);
        }

        $param2 = ['param2' => $request->query->get('param2')];

        $clickParams = array_merge($clickParams, $param2);

        /** @var Click $click */
        $click = $clickService->createClick($clickParams);

        return $this->redirectToRoute('click_success', [
            'clickUniqueId' => $click->getId()
        ]);
    }

    /**
     * @Route("/click/success/{clickUniqueId}", name="click_success")
     * @param $clickUniqueId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function clickSuccessAction($clickUniqueId)
    {
        $clickInfo = $this->get('app.click_service')
            ->getById($clickUniqueId);

        if (empty($clickInfo)) {
            throw new NotFoundHttpException('Click not found');
        }

        return $this->render('success.html.twig', [
            'clickInfo' => $clickInfo
        ]);
    }

    /**
     * @Route("/click/error/{clickUniqueId}", name="click_error")
     * @param $clickUniqueId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function clickErrorAction($clickUniqueId)
    {
        $clickService = $this->get('app.click_service');

        $click = $clickService->getById($clickUniqueId);

        if (empty($click)) {
            throw $this->createNotFoundException('Click not found');
        }

        $clickService->increaseErrorCount($clickUniqueId);

        return $this->render('error.html.twig', [
            'click' => $click,
        ]);
    }
}