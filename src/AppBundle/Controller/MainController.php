<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $existedClickList = $em->getRepository('AppBundle:Click')
            ->findAll();

        return $this->render('homepage.html.twig', [
            'existedClicks' => $existedClickList
        ]);
    }

    /**
     * @Route("/links/list", name="show_links_list")
     */
    public function showLinksTestAction()
    {
        return $this->render('links_list.html.twig');
    }
}
