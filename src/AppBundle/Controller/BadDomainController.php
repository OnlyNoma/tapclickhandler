<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BadDomainController extends Controller
{
    /**
     * @Route("/bad-domains/show", name="show_bad_domains")
     */
    public function showBadDomainsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $badDomains = $em->getRepository('AppBundle:BadDomains')->findAll();

        return $this->render('bad_domains.html.twig', [
            'badDomains' => $badDomains
        ]);
    }

    /**
     * @Route("/bad-domains/add/new", name="add_new_bad_domain")
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addBadDomainsAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->render('bad_domain_new_form.html.twig');
        }

        $domainName = $request->request->get('domain-name');

        if (empty($domainName)) {
            return new JsonResponse(['success' => false, 'message' => 'DomainName is empty']);
        }

        $badDomainService = $this->get('app.bad_domain_service');

        if ($badDomainService->isDomainExist($domainName)) {
            return new JsonResponse(['success' => false, 'message' => 'Domain already exist']);
        }

        $badDomainService->createNewBadDomain($domainName);

        return new JsonResponse(['success' => true, 'message' => 'New bad domain has successfully created']);
    }
}